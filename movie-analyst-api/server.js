// Get our dependencies
var express = require("express");
var app = express();
var mysql = require("mysql");
var port = process.env.PORT || 3000;
var connection = mysql.createConnection({
  host:
    process.env.DB_HOST ,
  user: process.env.DB_USER || "applicationuser",
  password: process.env.DB_PASS || "applicationuser",
  database: process.env.DB_NAME || "movie_db",
  maxConnection: 2,
});

connection.connect();

function getMovies(callback) {
  connection.query("SELECT * FROM movie_db.moviereview;", function (err, rows) {
    callback(err, rows);
  });
}

function getPublications(callback) {
  connection.query("SELECT * FROM movie_db.publication;", function (err, rows) {
    callback(err, rows);
  });
}

function getReviewers(callback) {
  connection.query("SELECT * FROM movie_db.reviewer;", function (err, rows) {
    callback(err, rows);
  });
}

// Implement the movies API endpoint
app.get("/api/movies", function (req, res) {
  getMovies(function (err, rows) {
    if (err) {
      res.json({ error: err });
    } else {
      res.json(rows);
    }
  });
});

app.get("/", function (req, res, next) {
  //now you can call the get-driver, passing a callback function
  getMovies(function (err, moviesResult) {
    //you might want to do something is err is not null...
    res.json(moviesResult);
  });
});

// Implement the reviewers API endpoint
app.get("/api/reviewers", function (req, res) {
  getReviewers(function (err, rows) {
    if (err) {
      res.json({ error: err });
    } else {
      res.json(rows);
    }
  });
});

// Implement the publications API endpoint
app.get("/api/publications", function (req, res) {
  getPublications(function (err, rows) {
    if (err) {
      res.json({ error: err });
    } else {
      res.json(rows);
    }
  });
});

// Implement the pending reviews API endpoint
app.get("/api/pending", function (req, res) {
  getMovies(function (err, rows) {
    if (err) {
      res.json({ error: err });
    } else {
      res.json(rows);
    }
  });
});
console.log("server listening through port: " + port);
// Launch our API Server and have it listen on port 3000.
app.listen(port);
module.exports = app;
