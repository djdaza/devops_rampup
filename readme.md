#!/bin/bash
touch ~/.bashrc # this ensure the bashrc file is created
source ~/.bashrc
cd ~
git clone https://djdaza@bitbucket.org/djdaza/devops_rampup.git
cd devops_rampup/
export BACK_HOST=192.168.100.139
chmod +x instance-config-front.sh
./instance-config-front.sh

#!/bin/bash
touch ~/.bashrc # this ensure the bashrc file is created
source ~/.bashrc
cd ~
git clone https://djdaza@bitbucket.org/djdaza/devops_rampup.git
cd devops_rampup/
chmod +x instance-config-back.sh
./instance-config-back.sh




-----------Docker----------------

#!/bin/bash
touch ~/.bashrc # this ensure the bashrc file is created
source ~/.bashrc

# Docker install
sudo apt-get -y update
sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin 

# Project start
cd ~
git clone https://djdaza@bitbucket.org/djdaza/devops_rampup.git
cd devops_rampup/
export BACK_HOST=192.168.100.139
docker-compose up movie-analyst-ui -d