#!/bin/bash

# Install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
source ~/.bashrc

nvm install --lts
nvm use --lts

# Install pm2
npm install -g pm2

# Install dependencies
cd ~/devops_rampup/movie-analyst-api
npm install
cd ~/devops_rampup

# Start the server
pm2 start ecosystem-back.config.js --watch
