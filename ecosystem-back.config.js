module.exports = {
  apps : [{
    name: 'movie-api',
    script: './movie-analyst-api/server.js',
    watch: ['./movie-analyst-api/']
  }],
};
