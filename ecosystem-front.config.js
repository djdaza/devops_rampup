module.exports = {
  apps : [{
    name: 'movie-ui',
    script: './movie-analyst-ui/server.js',
    watch: './movie-analyst-ui/'
  }],
};
